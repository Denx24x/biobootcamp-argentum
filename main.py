import requests


data = dict()
for i in range(1, 35):
    response = requests.get('http://www.allelefrequencies.net/hla6006a.asp?page=' + str(i) + '&hla_locus=&hla_allele1=&hla_allele2=&hla_population=&hla_country=Russia&hla_source=&hla_dataset=&hla_region=&hla_ethnic=&hla_selection=&hla_pop_selection=&hla_order=order_1&hla_show=&hla_sample_size=&hla_sample_size_pattern=equal&hla_sample_year=&hla_sample_year_pattern=equal&hla_level=&hla_level_pattern=equal&hla_locus_type=Classical&standard=a').content.decode('utf-8')
    response = response[response.index('<div id="divGenDetail">'):response.index('<b>Notes:</b>')]
    response = response[response.index('<tr bgcolor="#ffffff">'):response.index('</table>')]
    was = False
    for i in response.split('\n'):
        if was:
            bf = i.strip()
            name = bf[:bf.index('<')]
            val = bf[bf.index("<td align='center'>") + len("<td align='center'>"):]
            if '(*)' not in val:
                
                    #val = val[val.index("<td align='right'>") + len("<td align='right'>"):]
                val = val[:val.index('&')]
            else:
                val = val[:val.index('<')]
            if not len(val.strip()):
                val = '0'
            val = float(val)
            #print(name, val)
            if name not in data:
                data[name] = (0, 0)
            data[name] = (data[name][0] + val, data[name][1] + 1)
            was = False

        if i.strip()[:3] == '<a ' and 'See lower resolution data of this allele' in i:
            was = True
            #print(i.strip())
arr = {i[0]: i[1][0] / i[1][1] for i in data.items()}
print(arr)