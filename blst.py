import requests
import json
from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastpCommandline
from io import StringIO
from Bio.Blast import NCBIXML
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
class Data:

    def __init__(self, id=0, type="", name="", epitope="", spos=0, epos=0, MHC=0):
        self.id = id
        self.name = name
        self.epitope = epitope
        self.startpos = spos
        self.endpos = epos
        self.MHC = MHC
        self.HLA = []
        self.Tcells = []
        self.Bcells = []
        self.lenght = len(self.epitope)
        self.human_homology = []
        self.covid_homology = []

    def __str__(self):
        return json.dumps({'human_homology': self.human_homology, 'covid_homology': self.covid_homology, 'id': self.id, 'name': self.name, 'epitope': self.epitope, 'startpos': self.startpos, 'endpos': self.endpos, 'MHC': self.MHC, 'HLA': self.HLA,'Tcells': self.Tcells, 'Bcells': self.Bcells})
    
    def load_json(self, data):
        jsn = json.loads(data)
        if 'covid_homology' in jsn:
            self.covid_homology = jsn['covid_homology']
        else:
            self.covid_homology = []
        self.id = jsn['id']
        self.human_homology =  jsn['human_homology']
        self.name = jsn['name']
        self.epitope = jsn['epitope']
        self.startpos = jsn['startpos']
        self.endpos = jsn['endpos']
        self.MHC = jsn['MHC']
        self.HLA = jsn['HLA']
        self.Tcells = jsn['Tcells']
        self.Bcells = jsn['Bcells']
        return self

    def get_NLA_list(self):
        response = requests.get("https://www.iedb.org/epitope/" + str(self.id)).content.decode('utf-8');
        response = response[response.find('var compiledData'):response.find('var receptorSummaryData')].strip()
        response = response[response.index('{'):-1]
        data = json.loads(response)
        for i in data["data"]:
            if i["headers"]["main"] == "MHC Ligand Assay(s)":
                for g in i["data"]:
                    if int(g["positive_count"]) > 0:
                        self.HLA.append((g["mhc_molecule"], int(g["positive_count"]), int(g["total_count"])))
            elif i["headers"]["main"] == "B Cell Assay(s)":
                for g in i["data"]:
                    if int(g["positive_count"]) > 0:
                        self.Bcells.append((g["assay_type"], int(g["positive_count"]), int(g["total_count"])))
            elif i["headers"]["main"] == "T Cell Assay(s)":
                for g in i["data"]:
                    if int(g["positive_count"]) > 0:
                        self.Tcells.append((g["assay_type"], int(g["positive_count"]), int(g["total_count"])))

    def get_human_data(self):   
        seq1 = SeqRecord(Seq(self.epitope), id="seq1")
        SeqIO.write(seq1, "seq1.fasta", "fasta")

        output = NcbiblastpCommandline(query="C:\\Users\\Den\\Desktop\\bio\\seq1.fasta", subject="C:\\Users\\Den\\Desktop\\bio\\proteom.fasta", outfmt=5, evalue=10000)()[0]
        

        blast_result_record = NCBIXML.read(StringIO(output))
        mx = -1
        mxtitle = 0
        for i in blast_result_record.alignments:
            v = 0
            for g in i.hsps:
                v = max(v, g.score)
            if v > mx:
                mx = v
                mxtitle = i.title
        self.human_homology.append((mxtitle,mx))
        print(mx, mxtitle)
        """
        record = NCBIWWW.qblast('blastp', 'refseq_protein', self.epitope, entrez_query="txid9606[ORGN]", short_query=True, hitlist_size=5)
        records = NCBIXML.read(record)
        #print(records.alignments)
        for i in records.alignments:
            print(i.title)
            for g in i.hsps:
            #    print(g)
                #print(g.title)
                #print(g.expect)
                print(g.match)
            #    print('---')
        """


#
# аффинность MHC-MHC2 -
# гомогенность sarscov2 ?
# гомогенность человека -
# длина -
# покрытие различных HLA с учетом распространенности -
#

filt = open('final_data.txt', 'r')
out = open('final_epitope.txt', 'w')
fdata = [Data().load_json(i) for i in filt]
fdata.sort(key=lambda x: (max([-i[1] for i in x.covid_homology] + [0])))
for i in range(60):
    out.write(fdata[i].epitope + ' ' + fdata[i].name + '\n')
out.close()
"""
#out = open('data_filtered.txt', 'w')

data = [Data().load_json(i) for i in inp]
for i in data:
    if not len(i.human_homology) and len(i.HLA):
        i.get_human_data()
        out.write(str(i) + '\n')
out.close()
"""
#print(len([i for i in data if  len(i.HLA) and not len(i.human_homology)]))
"""
for i in data:
    i.get_human_data()
    out.write(str(i) + '\n')
out.close()
"""

#print(len(data))
#print(len([i for i in data if len(i.Bcells) and len(i.HLA)]))

"""
out = open('all_data.txt', 'w')
data = [list(map(lambda x: x.strip()[1:-1], i.split(','))) for i in open('epitope_all.csv', 'r').readlines()]

arr = [Data(i[0], i[1], i[9], i[2], i[5], i[6], 1)  for i in data]
for i in arr:
    i.get_NLA_list()
    out.write(str(i) + '\n')
out.close()
"""
